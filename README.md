Home page
---------

* [**cf-python**](http://cfpython.bitbucket.org)

Online documentation
--------------------

* Documentation for the latest stable release: Version 0.9.6.1
  [**HTML**](http://cfpython.bitbucket.org/docs/0.9.6/build/index.html),
  [**PDF**](http://cfpython.bitbucket.org/docs/0.9.6/build_pdf/cf-python.pdf)

* Documentation for previous releases:
  [**Archive**](http://cfpython.bitbucket.org/docs/archive.html)

Dependencies
------------

* At present, the package only runs under
  [**Linux**](http://en.wikipedia.org/wiki/Linux). However, the
  operating system dependencies are few (relating to the management of
  memory and open files) and most likely easily remedied for other
  operating systems.

* Requires a [**python**](http://www.python.org) version from 2.6 up
  to, but not including, 3.0.
 
* Requires the [**python numpy package**](http://www.numpy.org) at
  version 1.6 or newer.

* Requires the [**python netCDF4
  package**](http://code.google.com/p/netcdf4-python) at version 0.9.3
  or newer. This package requires the
  [**netCDF**](http://www.unidata.ucar.edu/software/netcdf),
  [**HDF5**](http://www.hdfgroup.org/HDF5) and
  [**zlib**](ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4)
  libraries.

* Requires [**UNIDATA's Udunits-2
  package**](http://www.unidata.ucar.edu/software/udunits)
  This is a library which provides support for units of physical
  quantities.

Quick installation using the distutils package
----------------------------------------------

1.  Download the required cf package from
    <https://bitbucket.org/cfpython/cf-python/downloads>
   
2.  Unpack it with (changing the version number as appropriate):
   
        tar zxvf cf-0.9.6.1.tar.gz
   
3.  Within the newly created directory 'cf-0.9.6.1' (changing the
    version number as appropriate) run one of the following:
   
    *  To install the cf package to a central location:
       
            python setup.py install
       
    *  To install the cf package to a locally to the user in a default
       location:
       
            python setup.py install --user
       
    *  To install the cf package in the <directory> of your choice:
       
            python setup.py install --home=<directory>

Quick installation without using the distutils package
------------------------------------------------------

1. Download the required cf package from
   <https://bitbucket.org/cfpython/cf-python/downloads>

2. Within a directory in your PYTHONPATH, unpack it with (changing the
   version number as appropriate):

        tar zxvf cf-0.9.6.1.tar.gz

cf2cf
-----

The command line utility for aggregating CF compliant netCDF files and
its man page are in the directory ``scripts``. If installing with the
distutils package, the script (``cf/scripts/cf2cf``) will be copied
automatically to a location in the environment's PATH, but the man
page (``cf/scripts/man1/cf2cf.1``) will need to be copied manually to a
location in the environment's MANPATH.

pp2cf
-----

The command line utility for converting PP files to CF compliant
netCDF files and its man page are in the directory ``scripts``. If
installing with the distutils package, the script
(``cf/scripts/pp2cf``) will be copied automatically to a location in
the environment's PATH, but the man page (``cf/scripts/man1/pp2cf.1``)
will need to be copied manually to a location in the environment's
MANPATH.

Unit tests
----------
The unit test scripts are in the ``test`` directory.

Downloaded documentation
------------------------

* HTML

        cf/docs/build/index.html

* PDF

        cf/docs/build_pdf/cf_python.pdf

Code license
------------

* [**MIT License**](http://opensource.org/licenses/mit-license.php)

    * Permission is hereby granted, free of charge, to any person
      obtaining a copy of this software and associated documentation
      files (the "Software"), to deal in the Software without
      restriction, including without limitation the rights to use,
      copy, modify, merge, publish, distribute, sublicense, and/or
      sell copies of the Software, and to permit persons to whom the
      Software is furnished to do so, subject to the following
      conditions:

    * The above copyright notice and this permission notice shall be
      included in all copies or substantial portions of the Software.

    * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
      FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
      OTHER DEALINGS IN THE SOFTWARE.
