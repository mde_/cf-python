cf.Filearray
============

.. autoclass:: cf.FileArray

   **FileArray attributes**
   
   .. autosummary::
      :nosignatures:

      ~cf.FileArray.dtype
      ~cf.FileArray.ndim
      ~cf.FileArray.shape
      ~cf.FileArray.size

   **FileArray methods**
   
   .. autosummary::
      :nosignatures:

      ~cf.FileArray.copy
