cf.Units
========

.. autoclass:: cf.Units

   **Units attributes**

   .. autosummary::
      :nosignatures:

      ~cf.Units.calendar
      ~cf.Units.islatitude
      ~cf.Units.islongitude
      ~cf.Units.ispressure
      ~cf.Units.isreftime
      ~cf.Units.istime
      ~cf.Units.units

   **Units methods**

   .. autosummary::
      :nosignatures:

      ~cf.Units.change_reftime_units
      ~cf.Units.conform
      ~cf.Units.copy
      ~cf.Units.dump
      ~cf.Units.equals
      ~cf.Units.equivalent
      ~cf.Units.format
      ~cf.Units.log
