.. _class:

Classes
=======


Field class
-----------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   ~cf.Field		              

Field component classes
-----------------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   ~cf.AncillaryVariables
   ~cf.CellMeasure
   ~cf.CellMethods
   ~cf.Coordinate
   ~cf.CoordinateBounds
   ~cf.Data
   ~cf.Flags
   ~cf.Space
   ~cf.Transform
   ~cf.Units

Miscellaneous classes
---------------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   ~cf.Comparison     
   ~cf.CoordinateList
   ~cf.FieldList

Data component classes
----------------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   ~cf.Partition
   ~cf.PartitionMatrix

Base classes
------------

.. autosummary::
   :nosignatures:
   :toctree: classes/

   ~cf.CfDict       
   ~cf.CfList         
   ~cf.Variable       
   ~cf.VariableList


.. _inheritance_diagrams:

Inheritance diagram
-------------------

The classes defined by the cf package inherit as follows:

.. image:: images/inheritance.png

.. commented out
  .. inheritance-diagram:: cf.Data
                           cf.Flags	
                           cf.Units
                           cf.Field
                           cf.CellMeasure
                           cf.Coordinate
                           cf.CoordinateBounds
     		           cf.Variable
                           cf.CfList
                           cf.CfDict
  			   cf.VariableList
     			   cf.FieldList
     			   cf.AncillaryVariables
                           cf.CoordinateList
                           cf.CellMethods
    			   cf.Space		 
                           cf.Transform
     :parts: 1


This inheritance diagram shows, for example:

* A :class:`.Field` object and a :class:`.Coordinate` both have all of
  the features of a :class:`.Variable` object, e.g. they may have a
  data array and CF attributes.

* A :class:`.FieldList` object is an iterable [1]_ which supports
  efficient element access using integer indices and defines a
  ``len()`` method that returns the length of the sequence.

* A :class:`.Space` object supports arbitrary, hashable key lookups
  and implements dictionary-like methods, such as ``pop()``.

.. rubric:: Footnotes

.. [1] An object capable of returning its members one at a time.
