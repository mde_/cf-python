\select@language {english}
\contentsline {part}{I\hspace {1em}Introduction}{1}{part.1}
\contentsline {part}{II\hspace {1em}Getting started}{5}{part.2}
\contentsline {chapter}{\numberline {1}A first example}{7}{chapter.1}
\contentsline {chapter}{\numberline {2}Further examples}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Reading}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Writing}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}CF properties and attributes}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}Selecting fields}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}The data array}{13}{section.2.5}
\contentsline {section}{\numberline {2.6}Coordinates}{13}{section.2.6}
\contentsline {section}{\numberline {2.7}Creating fields}{14}{section.2.7}
\contentsline {part}{III\hspace {1em}Reference manual}{15}{part.3}
\contentsline {chapter}{\numberline {3}Field structure}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}CF properties and attributes}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}Space structure}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Dimensionality}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Components}{21}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Field list}{21}{section.3.3}
\contentsline {section}{\numberline {3.4}Field versus field list}{22}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Attributes and methods}{22}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Looping}{22}{subsection.3.4.2}
\contentsline {chapter}{\numberline {4}Units}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Assignment}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Time units}{25}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Calendar}{26}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}Changing units}{26}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Changing the calendar}{27}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}Equality and equivalence of units}{27}{section.4.4}
\contentsline {section}{\numberline {4.5}Coordinate units}{27}{section.4.5}
\contentsline {chapter}{\numberline {5}Field manipulation}{29}{chapter.5}
\contentsline {section}{\numberline {5.1}Data array}{29}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Data mask}{29}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Conversion to a numpy array}{30}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Copying}{30}{section.5.2}
\contentsline {section}{\numberline {5.3}Subspacing}{30}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Indexing}{31}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Coordinate values}{31}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Selection}{32}{section.5.4}
\contentsline {section}{\numberline {5.5}Aggregation}{33}{section.5.5}
\contentsline {section}{\numberline {5.6}Assignment}{33}{section.5.6}
\contentsline {section}{\numberline {5.7}Arithmetic and comparison}{34}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Broadcasting}{34}{subsection.5.7.1}
\contentsline {subsection}{\numberline {5.7.2}What a field may be combined with}{34}{subsection.5.7.2}
\contentsline {subsection}{\numberline {5.7.3}Resulting metadata}{35}{subsection.5.7.3}
\contentsline {subsection}{\numberline {5.7.4}Overloaded operators}{36}{subsection.5.7.4}
\contentsline {section}{\numberline {5.8}Manipulation routines}{37}{section.5.8}
\contentsline {section}{\numberline {5.9}Manipulating other variables}{37}{section.5.9}
\contentsline {subsection}{\numberline {5.9.1}Coordinates}{38}{subsection.5.9.1}
\contentsline {chapter}{\numberline {6}Large Amounts of Massive Arrays (LAMA)}{39}{chapter.6}
\contentsline {section}{\numberline {6.1}Reading from files}{39}{section.6.1}
\contentsline {section}{\numberline {6.2}Copying}{39}{section.6.2}
\contentsline {section}{\numberline {6.3}Aggregation}{40}{section.6.3}
\contentsline {section}{\numberline {6.4}Subspacing}{40}{section.6.4}
\contentsline {section}{\numberline {6.5}Speed and memory management}{40}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}Temporary files}{41}{subsection.6.5.1}
\contentsline {subsection}{\numberline {6.5.2}Partitioning}{41}{subsection.6.5.2}
\contentsline {chapter}{\numberline {7}Functions}{43}{chapter.7}
\contentsline {section}{\numberline {7.1}Input and output}{43}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}cf.dump}{43}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}cf.read}{43}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}cf.write}{45}{subsection.7.1.3}
\contentsline {section}{\numberline {7.2}Aggregation}{46}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}cf.aggregate}{46}{subsection.7.2.1}
\contentsline {section}{\numberline {7.3}Mathematical operations}{47}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}cf.collapse}{47}{subsection.7.3.1}
\contentsline {section}{\numberline {7.4}Comparison}{47}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}cf.equals}{48}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}cf.eq}{48}{subsection.7.4.2}
\contentsline {subsection}{\numberline {7.4.3}cf.ge}{49}{subsection.7.4.3}
\contentsline {subsection}{\numberline {7.4.4}cf.gt}{49}{subsection.7.4.4}
\contentsline {subsection}{\numberline {7.4.5}cf.le}{50}{subsection.7.4.5}
\contentsline {subsection}{\numberline {7.4.6}cf.lt}{50}{subsection.7.4.6}
\contentsline {subsection}{\numberline {7.4.7}cf.ne}{50}{subsection.7.4.7}
\contentsline {subsection}{\numberline {7.4.8}cf.set}{51}{subsection.7.4.8}
\contentsline {subsection}{\numberline {7.4.9}cf.wi}{51}{subsection.7.4.9}
\contentsline {subsection}{\numberline {7.4.10}cf.wo}{52}{subsection.7.4.10}
\contentsline {section}{\numberline {7.5}Retrieval and setting of constants}{52}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}cf.ATOL}{52}{subsection.7.5.1}
\contentsline {subsection}{\numberline {7.5.2}cf.CHUNKSIZE}{52}{subsection.7.5.2}
\contentsline {subsection}{\numberline {7.5.3}cf.FM\_THRESHOLD}{53}{subsection.7.5.3}
\contentsline {subsection}{\numberline {7.5.4}cf.RTOL}{53}{subsection.7.5.4}
\contentsline {chapter}{\numberline {8}Classes}{55}{chapter.8}
\contentsline {section}{\numberline {8.1}Field class}{55}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}cf.Field}{55}{subsection.8.1.1}
\contentsline {subsubsection}{CF Properties}{56}{subsubsection*.23}
\contentsline {paragraph}{cf.Field.add\_offset}{56}{paragraph*.24}
\contentsline {paragraph}{cf.Field.calendar}{56}{paragraph*.26}
\contentsline {paragraph}{cf.Field.cell\_methods}{57}{paragraph*.28}
\contentsline {paragraph}{cf.Field.comment}{57}{paragraph*.30}
\contentsline {paragraph}{cf.Field.Conventions}{57}{paragraph*.32}
\contentsline {paragraph}{cf.Field.\_FillValue}{57}{paragraph*.34}
\contentsline {paragraph}{cf.Field.flag\_masks}{58}{paragraph*.36}
\contentsline {paragraph}{cf.Field.flag\_meanings}{58}{paragraph*.38}
\contentsline {paragraph}{cf.Field.flag\_values}{59}{paragraph*.40}
\contentsline {paragraph}{cf.Field.history}{59}{paragraph*.42}
\contentsline {paragraph}{cf.Field.institution}{59}{paragraph*.44}
\contentsline {paragraph}{cf.Field.leap\_month}{59}{paragraph*.46}
\contentsline {paragraph}{cf.Field.leap\_year}{60}{paragraph*.48}
\contentsline {paragraph}{cf.Field.long\_name}{60}{paragraph*.50}
\contentsline {paragraph}{cf.Field.missing\_value}{60}{paragraph*.52}
\contentsline {paragraph}{cf.Field.month\_lengths}{61}{paragraph*.54}
\contentsline {paragraph}{cf.Field.references}{61}{paragraph*.56}
\contentsline {paragraph}{cf.Field.scale\_factor}{61}{paragraph*.58}
\contentsline {paragraph}{cf.Field.source}{62}{paragraph*.60}
\contentsline {paragraph}{cf.Field.standard\_error\_multiplier}{62}{paragraph*.62}
\contentsline {paragraph}{cf.Field.standard\_name}{62}{paragraph*.64}
\contentsline {paragraph}{cf.Field.title}{63}{paragraph*.66}
\contentsline {paragraph}{cf.Field.units}{63}{paragraph*.68}
\contentsline {paragraph}{cf.Field.valid\_max}{63}{paragraph*.70}
\contentsline {paragraph}{cf.Field.valid\_min}{63}{paragraph*.72}
\contentsline {paragraph}{cf.Field.valid\_range}{64}{paragraph*.74}
\contentsline {subsubsection}{Data attributes}{64}{subsubsection*.76}
\contentsline {paragraph}{cf.Field.array}{64}{paragraph*.77}
\contentsline {paragraph}{cf.Field.Data}{65}{paragraph*.79}
\contentsline {paragraph}{cf.Field.dtype}{65}{paragraph*.81}
\contentsline {paragraph}{cf.Field.\_FillValue}{65}{paragraph*.83}
\contentsline {paragraph}{cf.Field.first\_datum}{66}{paragraph*.85}
\contentsline {paragraph}{cf.Field.hardmask}{66}{paragraph*.87}
\contentsline {paragraph}{cf.Field.isscalar}{66}{paragraph*.89}
\contentsline {paragraph}{cf.Field.last\_datum}{67}{paragraph*.91}
\contentsline {paragraph}{cf.Field.mask}{67}{paragraph*.93}
\contentsline {paragraph}{cf.Field.ndim}{67}{paragraph*.95}
\contentsline {paragraph}{cf.Field.shape}{67}{paragraph*.97}
\contentsline {paragraph}{cf.Field.size}{68}{paragraph*.99}
\contentsline {paragraph}{cf.Field.Units}{68}{paragraph*.101}
\contentsline {paragraph}{cf.Field.varray}{68}{paragraph*.103}
\contentsline {paragraph}{cf.Field.ancillary\_variables}{68}{paragraph*.105}
\contentsline {paragraph}{cf.Field.Flags}{69}{paragraph*.107}
\contentsline {paragraph}{cf.Field.hasData}{69}{paragraph*.109}
\contentsline {paragraph}{cf.Field.properties}{69}{paragraph*.111}
\contentsline {paragraph}{cf.Field.space}{69}{paragraph*.113}
\contentsline {subsubsection}{Methods}{70}{subsubsection*.115}
\contentsline {paragraph}{cf.Field.binary\_mask}{70}{paragraph*.116}
\contentsline {paragraph}{cf.Field.chunk}{70}{paragraph*.118}
\contentsline {paragraph}{cf.Field.clip}{71}{paragraph*.120}
\contentsline {paragraph}{cf.Field.coord}{71}{paragraph*.122}
\contentsline {paragraph}{cf.Field.copy}{72}{paragraph*.124}
\contentsline {paragraph}{cf.Field.cos}{72}{paragraph*.126}
\contentsline {paragraph}{cf.Field.delprop}{73}{paragraph*.128}
\contentsline {paragraph}{cf.Field.dump}{73}{paragraph*.130}
\contentsline {paragraph}{cf.Field.equals}{73}{paragraph*.132}
\contentsline {paragraph}{cf.Field.expand\_dims}{74}{paragraph*.134}
\contentsline {paragraph}{cf.Field.flip}{74}{paragraph*.136}
\contentsline {paragraph}{cf.Field.getprop}{75}{paragraph*.138}
\contentsline {paragraph}{cf.Field.finalize}{75}{paragraph*.140}
\contentsline {paragraph}{cf.Field.hasprop}{76}{paragraph*.142}
\contentsline {paragraph}{cf.Field.identity}{76}{paragraph*.144}
\contentsline {paragraph}{cf.Field.indices}{77}{paragraph*.146}
\contentsline {paragraph}{cf.Field.match}{77}{paragraph*.148}
\contentsline {paragraph}{cf.Field.name}{79}{paragraph*.150}
\contentsline {paragraph}{cf.Field.override\_units}{79}{paragraph*.152}
\contentsline {paragraph}{cf.Field.setitem}{80}{paragraph*.154}
\contentsline {paragraph}{cf.Field.setmask}{81}{paragraph*.156}
\contentsline {paragraph}{cf.Field.setprop}{82}{paragraph*.158}
\contentsline {paragraph}{cf.Field.sin}{82}{paragraph*.160}
\contentsline {paragraph}{cf.Field.squeeze}{83}{paragraph*.162}
\contentsline {paragraph}{cf.Field.subset}{83}{paragraph*.164}
\contentsline {paragraph}{cf.Field.subspace}{84}{paragraph*.166}
\contentsline {paragraph}{cf.Field.transpose}{84}{paragraph*.168}
\contentsline {paragraph}{cf.Field.unsqueeze}{85}{paragraph*.170}
\contentsline {section}{\numberline {8.2}Field component classes}{85}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}cf.AncillaryVariables}{85}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}cf.CellMeasure}{92}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}cf.CellMethods}{108}{subsection.8.2.3}
\contentsline {subsection}{\numberline {8.2.4}cf.Coordinate}{113}{subsection.8.2.4}
\contentsline {subsection}{\numberline {8.2.5}cf.CoordinateBounds}{131}{subsection.8.2.5}
\contentsline {subsection}{\numberline {8.2.6}cf.Data}{148}{subsection.8.2.6}
\contentsline {subsection}{\numberline {8.2.7}cf.Flags}{162}{subsection.8.2.7}
\contentsline {subsection}{\numberline {8.2.8}cf.Space}{165}{subsection.8.2.8}
\contentsline {subsection}{\numberline {8.2.9}cf.Transform}{173}{subsection.8.2.9}
\contentsline {subsection}{\numberline {8.2.10}cf.Units}{176}{subsection.8.2.10}
\contentsline {section}{\numberline {8.3}Miscellaneous classes}{184}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}cf.Comparison}{184}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}cf.CoordinateList}{187}{subsection.8.3.2}
\contentsline {subsection}{\numberline {8.3.3}cf.FieldList}{193}{subsection.8.3.3}
\contentsline {section}{\numberline {8.4}Data component classes}{199}{section.8.4}
\contentsline {subsection}{\numberline {8.4.1}cf.Partition}{199}{subsection.8.4.1}
\contentsline {subsection}{\numberline {8.4.2}cf.PartitionArray}{204}{subsection.8.4.2}
\contentsline {subsection}{\numberline {8.4.3}cf.Filearray}{209}{subsection.8.4.3}
\contentsline {section}{\numberline {8.5}Base classes}{211}{section.8.5}
\contentsline {subsection}{\numberline {8.5.1}cf.CfDict}{211}{subsection.8.5.1}
\contentsline {subsection}{\numberline {8.5.2}cf.CfList}{212}{subsection.8.5.2}
\contentsline {subsection}{\numberline {8.5.3}cf.Variable}{214}{subsection.8.5.3}
\contentsline {subsection}{\numberline {8.5.4}cf.VariableList}{231}{subsection.8.5.4}
\contentsline {section}{\numberline {8.6}Inheritance diagrams}{237}{section.8.6}
\contentsline {chapter}{\numberline {9}Constants}{239}{chapter.9}
\contentsline {section}{\numberline {9.1}cf.CONSTANTS}{239}{section.9.1}
\contentsline {part}{IV\hspace {1em}Indices and tables}{241}{part.4}
\contentsline {chapter}{Index}{245}{section*.654}
