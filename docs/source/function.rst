.. _function:

Functions
=========

Input and output
----------------

.. autosummary::
   :nosignatures:
   :toctree: generated/

   ~cf.close_files
   ~cf.close_one_file
   ~cf.dump
   ~cf.open_files
   ~cf.open_files_threshold_exceeded
   ~cf.pickle
   ~cf.read 
   ~cf.write
   ~cf.unpickle

Aggregation
-----------

.. autosummary::
   :nosignatures:
   :toctree: generated/

   ~cf.aggregate

Mathematical operations
-----------------------

.. autosummary::
   :nosignatures:
   :toctree: generated/

   ~cf.clip
   ~cf.collapse
   ~cf.cos
   ~cf.sin

Comparison
----------

.. autosummary::
   :nosignatures:
   :toctree: generated/

   ~cf.equals
   ~cf.eq
   ~cf.ge
   ~cf.gt
   ~cf.le
   ~cf.lt
   ~cf.ne
   ~cf.set
   ~cf.wi
   ~cf.wo

Retrieval and setting of constants
----------------------------------

.. autosummary::
   :nosignatures:
   :toctree: generated/

   ~cf.ATOL
   ~cf.CHUNKSIZE
   ~cf.FM_THRESHOLD
   ~cf.MINNCFM
   ~cf.OF_FRACTION
   ~cf.RTOL
   ~cf.TEMPDIR
