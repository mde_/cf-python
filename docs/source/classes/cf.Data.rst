cf.Data
=======

.. autoclass:: cf.Data

   **Data attributes**
   
   .. autosummary::
      :nosignatures:

      ~cf.Data.array
      ~cf.Data.direction
      ~cf.Data.dtype
      ~cf.Data._FillValue
      ~cf.Data.first_datum
      ~cf.Data.hardmask
      ~cf.Data.ismasked
      ~cf.Data.isscalar
      ~cf.Data.last_datum
      ~cf.Data.mask
      ~cf.Data.ndim
      ~cf.Data.order
      ~cf.Data.partitions
      ~cf.Data.pdims
      ~cf.Data.pshape
      ~cf.Data.psize
      ~cf.Data.shape
      ~cf.Data.size
      ~cf.Data.Units
      ~cf.Data.varray

   **Data methods**
   
   .. autosummary::
      :nosignatures:

      ~cf.Data.add_partitions
      ~cf.Data.all
      ~cf.Data.any
      ~cf.Data.binary_mask
      ~cf.Data.change_dimension_names
      ~cf.Data.chunk
      ~cf.Data.clip
      ~cf.Data.copy
      ~cf.Data.cos
      ~cf.Data.dump
      ~cf.Data.equals
      ~cf.Data.expand_dims
      ~cf.Data.expand_partition_dims
      ~cf.Data.flat
      ~cf.Data.flip
      ~cf.Data.func
      ~cf.Data.iterindices
      ~cf.Data.new_dimension_name
      ~cf.Data.override_units
      ~cf.Data.partition_boundaries
      ~cf.Data.save_to_disk
      ~cf.Data.setitem
      ~cf.Data.setmask
      ~cf.Data.sin
      ~cf.Data.squeeze
      ~cf.Data.to_disk
      ~cf.Data.to_memory
      ~cf.Data.transpose
