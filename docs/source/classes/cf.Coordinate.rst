cf.Coordinate
=============

.. autoclass:: cf.Coordinate
   :members:
   :inherited-members:

   **Coordinate CF properties**

   .. autosummary::

      ~cf.Coordinate.add_offset
      ~cf.Coordinate.axis
      ~cf.Coordinate.calendar
      ~cf.Coordinate.comment
      ~cf.Coordinate._FillValue
      ~cf.Coordinate.history
      ~cf.Coordinate.leap_month
      ~cf.Coordinate.leap_year
      ~cf.Coordinate.long_name
      ~cf.Coordinate.missing_value
      ~cf.Coordinate.month_lengths
      ~cf.Coordinate.positive
      ~cf.Coordinate.scale_factor
      ~cf.Coordinate.standard_name
      ~cf.Coordinate.units
      ~cf.Coordinate.valid_max
      ~cf.Coordinate.valid_min
      ~cf.Coordinate.valid_range

   **Coordinate data attributes**
   
   .. autosummary::

      ~cf.Coordinate.array
      ~cf.Coordinate.Data
      ~cf.Coordinate.dtype
      ~cf.Coordinate._FillValue
      ~cf.Coordinate.first_datum
      ~cf.Coordinate.hardmask
      ~cf.Coordinate.isscalar
      ~cf.Coordinate.last_datum
      ~cf.Coordinate.mask
      ~cf.Coordinate.ndim
      ~cf.Coordinate.shape
      ~cf.Coordinate.size
      ~cf.Coordinate.Units
      ~cf.Coordinate.varray

   **Coordinate miscellaneous attributes**
   
   .. autosummary::

      ~cf.Coordinate.bounds
      ~cf.Coordinate.climatology
      ~cf.Coordinate.hasData
      ~cf.Coordinate.isbounded
      ~cf.Coordinate.properties
      ~cf.Coordinate.transform

   **Coordinate methods**

   .. autosummary::
      :nosignatures:

      ~cf.Coordinate.binary_mask
      ~cf.Coordinate.chunk
      ~cf.Coordinate.contiguous
      ~cf.Coordinate.copy
      ~cf.Coordinate.cos
      ~cf.Coordinate.delprop
      ~cf.Coordinate.dump
      ~cf.Coordinate.equals
      ~cf.Coordinate.expand_dims
      ~cf.Coordinate.flip
      ~cf.Coordinate.getprop
      ~cf.Coordinate.hasprop
      ~cf.Coordinate.identity
      ~cf.Coordinate.match
      ~cf.Coordinate.name
      ~cf.Coordinate.override_units
      ~cf.Coordinate.setitem
      ~cf.Coordinate.setmask
      ~cf.Coordinate.setprop
      ~cf.Coordinate.sin
      ~cf.Coordinate.squeeze
      ~cf.Coordinate.subset
      ~cf.Coordinate.subspace
      ~cf.Coordinate.transpose
