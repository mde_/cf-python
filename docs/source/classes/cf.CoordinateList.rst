cf.CoordinateList
=================

.. autoclass:: cf.CoordinateList

   **CoordinateList methods** (undocumented methods behave exactly as their
   counterparts in a built-in list)
   
   .. autosummary::
      :nosignatures:

      ~cf.CoordinateList.append
      ~cf.CoordinateList.copy
      ~cf.CoordinateList.count
      ~cf.CoordinateList.delprop
      ~cf.CoordinateList.dump
      ~cf.CoordinateList.equals
      ~cf.CoordinateList.extend
      ~cf.CoordinateList.getprop
      ~cf.CoordinateList.hasprop
      ~cf.CoordinateList.index
      ~cf.CoordinateList.insert
      ~cf.CoordinateList.match
      ~cf.CoordinateList.name
      ~cf.CoordinateList.pop
      ~cf.CoordinateList.remove
      ~cf.CoordinateList.reverse
      ~cf.CoordinateList.setprop
      ~cf.CoordinateList.subset
      ~cf.CoordinateList.subspace

   **CoordinateList methods**
   
   .. autosummary::
      :nosignatures:
