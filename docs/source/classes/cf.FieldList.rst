cf.FieldList
============

.. autoclass:: cf.FieldList

   **FieldList methods** (undocumented methods behave exactly as their
   counterparts in a built-in list)

   .. autosummary::
      :nosignatures:

      ~cf.FieldList.append
      ~cf.FieldList.coord
      ~cf.FieldList.copy
      ~cf.FieldList.count
      ~cf.FieldList.delprop
      ~cf.FieldList.dump
      ~cf.FieldList.equals
      ~cf.FieldList.extend
      ~cf.FieldList.getprop
      ~cf.FieldList.hasprop
      ~cf.FieldList.index
      ~cf.FieldList.insert
      ~cf.FieldList.match
      ~cf.FieldList.name
      ~cf.FieldList.override_units
      ~cf.FieldList.pop
      ~cf.FieldList.remove
      ~cf.FieldList.reverse
      ~cf.FieldList.set_equals
      ~cf.FieldList.setprop
      ~cf.FieldList.sort
      ~cf.FieldList.squeeze
      ~cf.FieldList.subset
      ~cf.FieldList.subspace
      ~cf.FieldList.unsqueeze
