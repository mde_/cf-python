cf.CoordinateBounds
===================

.. autoclass:: cf.CoordinateBounds

   **CoordinateBounds CF Properties**
 
   .. autosummary::

      ~cf.CoordinateBounds.add_offset
      ~cf.CoordinateBounds.calendar
      ~cf.CoordinateBounds.comment
      ~cf.CoordinateBounds._FillValue
      ~cf.CoordinateBounds.history
      ~cf.CoordinateBounds.leap_month
      ~cf.CoordinateBounds.leap_year
      ~cf.CoordinateBounds.long_name
      ~cf.CoordinateBounds.missing_value
      ~cf.CoordinateBounds.month_lengths
      ~cf.CoordinateBounds.scale_factor
      ~cf.CoordinateBounds.standard_name
      ~cf.CoordinateBounds.units
      ~cf.CoordinateBounds.valid_max
      ~cf.CoordinateBounds.valid_min
      ~cf.CoordinateBounds.valid_range

   **CoordinateBounds data attributes**
   
   .. autosummary::

      ~cf.CoordinateBounds.array
      ~cf.CoordinateBounds.Data
      ~cf.CoordinateBounds.dtype
      ~cf.CoordinateBounds._FillValue
      ~cf.CoordinateBounds.first_datum
      ~cf.CoordinateBounds.hardmask
      ~cf.CoordinateBounds.isscalar
      ~cf.CoordinateBounds.last_datum
      ~cf.CoordinateBounds.mask
      ~cf.CoordinateBounds.ndim
      ~cf.CoordinateBounds.shape
      ~cf.CoordinateBounds.size
      ~cf.CoordinateBounds.Units
      ~cf.CoordinateBounds.varray

   **CoordinateBounds miscellaneous attributes**
   
   .. autosummary::

      ~cf.CoordinateBounds.hasData
      ~cf.CoordinateBounds.properties

   **CoordinateBounds methods**
   
   .. autosummary::
      :nosignatures:

      ~cf.CoordinateBounds.binary_mask
      ~cf.CoordinateBounds.chunk
      ~cf.CoordinateBounds.clip
      ~cf.CoordinateBounds.copy
      ~cf.CoordinateBounds.cos
      ~cf.CoordinateBounds.delprop
      ~cf.CoordinateBounds.dump
      ~cf.CoordinateBounds.equals
      ~cf.CoordinateBounds.expand_dims
      ~cf.CoordinateBounds.flip
      ~cf.CoordinateBounds.getprop
      ~cf.CoordinateBounds.hasprop
      ~cf.CoordinateBounds.identity
      ~cf.CoordinateBounds.match
      ~cf.CoordinateBounds.name
      ~cf.CoordinateBounds.override_units
      ~cf.CoordinateBounds.setitem
      ~cf.CoordinateBounds.setmask
      ~cf.CoordinateBounds.setprop
      ~cf.CoordinateBounds.sin
      ~cf.CoordinateBounds.squeeze
      ~cf.CoordinateBounds.subset  
      ~cf.CoordinateBounds.subspace
      ~cf.CoordinateBounds.transpose
