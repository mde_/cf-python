cf.VariableList
===============

.. autoclass:: cf.VariableList

   **VariableList methods** (undocumented methods behave exactly as their
   counterparts in a built-in list)

   .. autosummary::
      :nosignatures:

      ~cf.VariableList.append
      ~cf.VariableList.copy
      ~cf.VariableList.count
      ~cf.VariableList.delprop
      ~cf.VariableList.dump
      ~cf.VariableList.equals
      ~cf.VariableList.extend
      ~cf.VariableList.getprop
      ~cf.VariableList.hasprop
      ~cf.VariableList.index
      ~cf.VariableList.insert
      ~cf.VariableList.match
      ~cf.VariableList.name
      ~cf.VariableList.override_units
      ~cf.VariableList.pop
      ~cf.VariableList.remove
      ~cf.VariableList.reverse
      ~cf.VariableList.set_equals
      ~cf.VariableList.setprop
      ~cf.VariableList.sort
      ~cf.VariableList.subset
      ~cf.VariableList.subspace
