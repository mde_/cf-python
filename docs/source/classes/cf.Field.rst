cf.Field
========

.. autoclass:: cf.Field
   :no-members:
   :no-inherited-members:

CF Properties
-------------
 
   .. autosummary::
      :toctree: ../generated/

      ~cf.Field.add_offset
      ~cf.Field.calendar
      ~cf.Field.cell_methods
      ~cf.Field.comment
      ~cf.Field.Conventions
      ~cf.Field._FillValue
      ~cf.Field.flag_masks
      ~cf.Field.flag_meanings
      ~cf.Field.flag_values
      ~cf.Field.history
      ~cf.Field.institution
      ~cf.Field.leap_month
      ~cf.Field.leap_year
      ~cf.Field.long_name
      ~cf.Field.missing_value
      ~cf.Field.month_lengths
      ~cf.Field.references
      ~cf.Field.scale_factor
      ~cf.Field.source
      ~cf.Field.standard_error_multiplier
      ~cf.Field.standard_name
      ~cf.Field.title
      ~cf.Field.units
      ~cf.Field.valid_max
      ~cf.Field.valid_min
      ~cf.Field.valid_range

Data attributes
---------------
   
   .. autosummary::
      :toctree: ../generated/

      ~cf.Field.array
      ~cf.Field.Data
      ~cf.Field.dtype
      ~cf.Field._FillValue
      ~cf.Field.first_datum
      ~cf.Field.hardmask
      ~cf.Field.isscalar
      ~cf.Field.last_datum
      ~cf.Field.mask
      ~cf.Field.ndim
      ~cf.Field.shape
      ~cf.Field.size
      ~cf.Field.Units
      ~cf.Field.varray

   **Other attributes**
   
   .. autosummary::
      :toctree: ../generated/

      ~cf.Field.ancillary_variables
      ~cf.Field.Flags
      ~cf.Field.hasData
      ~cf.Field.properties
      ~cf.Field.space

Methods
-------
   
   .. autosummary::
      :nosignatures:
      :toctree: ../generated/

      ~cf.Field.binary_mask
      ~cf.Field.chunk
      ~cf.Field.clip
      ~cf.Field.coord
      ~cf.Field.copy
      ~cf.Field.cos
      ~cf.Field.delprop
      ~cf.Field.dump
      ~cf.Field.equals
      ~cf.Field.equivalent_data
      ~cf.Field.expand_dims
      ~cf.Field.flip
      ~cf.Field.getprop
      ~cf.Field.finalize
      ~cf.Field.hasprop
      ~cf.Field.identity
      ~cf.Field.indices
      ~cf.Field.match
      ~cf.Field.name
      ~cf.Field.override_units
      ~cf.Field.setitem
      ~cf.Field.setmask
      ~cf.Field.setprop
      ~cf.Field.sin
      ~cf.Field.squeeze
      ~cf.Field.subset  
      ~cf.Field.subspace
      ~cf.Field.transpose
      ~cf.Field.unsqueeze
