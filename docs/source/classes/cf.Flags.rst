cf.Flags
========

.. autoclass:: cf.Flags

   **Flags attributes**
   
   .. autosummary::
      :nosignatures:

      ~cf.Flags.flag_masks
      ~cf.Flags.flag_meanings
      ~cf.Flags.flag_values

   **Flags methods**
   
   .. autosummary::
      :nosignatures:

      ~cf.Flags.copy
      ~cf.Flags.dump
      ~cf.Flags.equals
      ~cf.Flags.sort
