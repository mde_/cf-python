cf.Space
========

.. autoclass:: cf.Space

   **Space attributes**
   
   .. autosummary::

      ~cf.Space.dimension_sizes
      ~cf.Space.dimensions
      ~cf.Space.transforms

   **Space methods** (undocumented methods behave exactly as their
   counterparts in a built-in dictionary)
        
   .. autosummary::
      :nosignatures:

      ~cf.Space.analyse 
      ~cf.Space.aux_coords
      ~cf.Space.cell_measures
      ~cf.Space.clear
      ~cf.Space.coord
      ~cf.Space.copy
      ~cf.Space.direction
      ~cf.Space.dump
      ~cf.Space.equals
      ~cf.Space.expand_dims
      ~cf.Space.get
      ~cf.Space.get_keys
      ~cf.Space.has_key
      ~cf.Space.insert_coordinate
      ~cf.Space.items
      ~cf.Space.iteritems
      ~cf.Space.iterkeys
      ~cf.Space.itervalues
      ~cf.Space.keys
      ~cf.Space.map_dims
      ~cf.Space.new_auxiliary
      ~cf.Space.new_dimension
      ~cf.Space.new_transform
      ~cf.Space.pop
      ~cf.Space.popitem
      ~cf.Space.remove_coordinate
      ~cf.Space.setdefault
      ~cf.Space.squeeze
      ~cf.Space.update
      ~cf.Space.values
