cf.Comparison
=============

.. autoclass:: cf.Comparison

   **Comparison methods**
   
   .. autosummary::
      :nosignatures:

      ~cf.Comparison.copy
      ~cf.Comparison.dump
      ~cf.Comparison.evaluate
