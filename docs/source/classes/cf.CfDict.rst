cf.CfDict
=========

.. autoclass:: cf.CfDict

   **CfDict methods** (undocumented methods behave exactly as their
   counterparts in a built-in dictionary)
   
   .. autosummary::
      :nosignatures:

      ~cf.CfDict.clear
      ~cf.CfDict.copy
      ~cf.CfDict.equals
      ~cf.CfDict.get
      ~cf.CfDict.get_keys
      ~cf.CfDict.has_key
      ~cf.CfDict.items
      ~cf.CfDict.iteritems
      ~cf.CfDict.iterkeys
      ~cf.CfDict.itervalues
      ~cf.CfDict.keys
      ~cf.CfDict.pop
      ~cf.CfDict.popitem
      ~cf.CfDict.setdefault
      ~cf.CfDict.update
      ~cf.CfDict.values
