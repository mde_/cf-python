cf.AncillaryVariables
=====================

.. autoclass:: cf.AncillaryVariables

   **AncillaryVariables methods** (undocumented methods behave exactly
   as their counterparts in a built-in list)

   .. autosummary::
      :nosignatures:

      ~cf.AncillaryVariables.append
      ~cf.AncillaryVariables.coord
      ~cf.AncillaryVariables.copy
      ~cf.AncillaryVariables.count
      ~cf.AncillaryVariables.delprop
      ~cf.AncillaryVariables.dump
      ~cf.AncillaryVariables.equals
      ~cf.AncillaryVariables.extend
      ~cf.AncillaryVariables.getprop
      ~cf.AncillaryVariables.hasprop
      ~cf.AncillaryVariables.index
      ~cf.AncillaryVariables.insert
      ~cf.AncillaryVariables.match
      ~cf.AncillaryVariables.name
      ~cf.AncillaryVariables.override_units
      ~cf.AncillaryVariables.pop
      ~cf.AncillaryVariables.remove
      ~cf.AncillaryVariables.reverse
      ~cf.AncillaryVariables.set_equals
      ~cf.AncillaryVariables.setprop
      ~cf.AncillaryVariables.sort
      ~cf.AncillaryVariables.squeeze
      ~cf.AncillaryVariables.subset
      ~cf.AncillaryVariables.subspace
      ~cf.AncillaryVariables.unsqueeze
