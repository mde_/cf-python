cf.Partition
============

.. autoclass:: cf.Partition

   **Partition attributes**
   
   .. autosummary::
      :nosignatures:

      ~cf.Partition.data
      ~cf.Partition.direction
      ~cf.Partition.in_memory
      ~cf.Partition.indices
      ~cf.Partition.isscalar
      ~cf.Partition.location
      ~cf.Partition.on_disk
      ~cf.Partition.order
      ~cf.Partition.part
      ~cf.Partition.shape

   **Partition methods**
        
   .. autosummary::
      :nosignatures:
      
      ~cf.Partition.close
      ~cf.Partition.conform
      ~cf.Partition.copy
      ~cf.Partition.iterarray_indices
      ~cf.Partition.itermaster_indices
      ~cf.Partition.new_part
      ~cf.Partition.to_disk
      ~cf.Partition.update_from
