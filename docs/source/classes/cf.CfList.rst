cf.CfList
=========

.. autoclass:: cf.CfList

   **CfList methods** (undocumented methods behave exactly as their
   counterparts in a built-in list)
   
   .. autosummary::
      :nosignatures:

      ~cf.CfList.append
      ~cf.CfList.copy
      ~cf.CfList.count
      ~cf.CfList.equals
      ~cf.CfList.extend
      ~cf.CfList.index
      ~cf.CfList.insert
      ~cf.CfList.pop
      ~cf.CfList.remove
      ~cf.CfList.reverse
