.. _constant:

Constants
=========

Useful constants are stored in the :data:`.CONSTANTS` dictionary.

.. toctree::
   :hidden:

   cf.CONSTANTS
