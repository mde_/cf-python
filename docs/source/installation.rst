Installation
============

Downloads, installation instructions and source code are all available
from the cf-python home page (http://cfpython.bitbucket.org).

The cf package may be downloaded from
https://bitbucket.org/cfpython/cf-python/downloads. Only the most
recent frozen version is currently supported.

The latest development source code may be browsed and downloaded from
https://bitbucket.org/cfpython/cf-python/src.

Installation instructions are in the README.md file contained with the
downloaded source code, which is also linked form the cf-python home
page (https://bitbucket.org/cfpython/cf-python/overview). This file
also details the cf package's dependencies.

Please raise any questions or problems at
https://bitbucket.org/cfpython/cf-python/issues.
