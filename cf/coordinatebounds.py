from .variable import Variable

# ====================================================================
#
# CoordinateBounds object
#
# ====================================================================

class CoordinateBounds(Variable):
    '''
    
A CF coordinate's bounds object containing cell boundaries or
intervals of climatological time. The parent coordinate's
`climatology` attribute indicates which type of bounds are present.

'''
    pass
#--- End: class
