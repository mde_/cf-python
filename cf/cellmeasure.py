from .variable import Variable

# ====================================================================
#
# CellMeasure object
#
# ====================================================================

class CellMeasure(Variable):
    '''
    
A CF cell measure construct containing information that is needed
about the size, shape or location of the field's cells.

It is a variable which contains a data array and metadata comprising
properties to describe the physical nature of the data.

The name of spatial measure being represented is stored in the
`measure` attribute.

'''
    # ----------------------------------------------------------------
    # Attribute: measure
    # ----------------------------------------------------------------
    @property
    def measure(self):
        '''

The name of spatial measure being represented.

**Examples**

>>> c.measure
'area'

'''
        return self._getter('measure')
    #--- End: for
    @measure.setter
    def measure(self, value): self._setter('measure', value)
    @measure.deleter
    def measure(self):        self._deleter('measure')

    def dump(self, id=None): 
        '''

Return a string containing a full description of the cell measure.

:Parameters:

    id: str, optional
       Set the common prefix of variable component names. By default
       the instance's class name is used.

:Returns:

    out : str
        A string containing the description.

**Examples**

>>> x = c.dump()
>>> print c.dump()
>>> print c.dump(id='area_measure')

'''
        if id is None:
            id = self.__class__.__name__       

        if hasattr(self, 'measure'):
            string = ['%s cell measure' % self.measure]
        elif hasattr(self.Units, 'units'):
            string = ['%s cell measure' % self.units]
        else:
            string = ['%s cell measure' % self.name(default='', long_name=True)]

        string.append(''.ljust(len(string[0]), '-'))
        
        string.append(super(CellMeasure, self).dump(id=id))
            
        return '\n'.join(string)
    #--- End: def

    def identity(self, default=None):
        '''

Return the cell measure's identity.

The idendity is the value of the `measure` attribute, or if that
doesn't exist the `standard_name` property or, if that does not exist,
the `id` attribute.

:Parameters:

    default : optional
        If none of `measure`, `standard_name` and `id` exist then
        return `default`. By default, `default` is None.

:Returns:

    out :
        The identity.

**Examples**

'''
        return getattr(self, 'measure', getattr(self, 'standard_name',
                                                getattr(self, 'id', default)))
    #--- End: def

    def name(self, long_name=None, ncvar=None, default=None):
        '''

'''  
        if hasattr(self, 'measure'):
            return self.measure
        
        if hasattr(self, 'standard_name'):
            return self.standard_name
        
        if long_name is not None and hasattr(self, 'long_name'):
            return self.long_name
        
        if ncvar is not None and hasattr(self, 'ncvar'):
            return self.ncvar
        
        return default
    #--- End: def

#--- End: class
